#API REST Proyecto final TechU practioner 2020-2
==============================================

Todo el código esta alojado en GitHub en el siguiente repositorio: [API REST adi] (https://bitbucket.org/jvargasy/bm-frontend/src/master/)

#Introducción
=============
Esta API está desarrollado para el proyecto Practitioner BBVA Perú segunda edición 2020. Aplicación web que realiza transferencias, envío correo electrónico, lista tarjetas, lista movimientos, muestra cuentas, busca cuentas, toda la funcionalidad a través de un logueo y si el usuario es nuevo realiza la afiliación y puede recuperar contraseñas.
Usa una API externa para mostrar el tipo de cambio ($) del día.

#Lista de comandos para puesta en marcha
========================================

  1. Versión del polymer : 1.6.0
  2. Versión del bower : 1.8.4
  3. Versión del node : 6.0.0
  4. Correr el servidor : polymer serve
  
#Arquitectura
========================================
   ![alt_text](readme/arquitectura.png)

#Funcionalidades
================
  1. Loguin: Logueo del usuario.
  2. Afiliar: Usuario que se da de alta en la Banca Móvil con username, password y correo eléctronico.
  3. Recuperar contraseña : Usuario que necesita recuperar su contraseña y este es enviado al correo      registrado al darse de alta en la Banca.
  4. Movimientos: Lista de los movimientos que realizó el usuario.
  5. Cuentas: Lista las cuentas del usuario.
  6. Tarjetas: Lista las tarjetas que tiene el usuario.
  8. Tipo de Cambio: Muestra el cambio del día en cuanto ingresa a la página de movimientos.
  7. Salir: Logout del usuario.
  
#Funcionalidades
================
    Diseño adaptativo a cualquier resolución de pantalla
   ![alt_text](readme/diseño.png)
